# Retrieve Parameters in JSP

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

This project will get data from the url and show it in the page.

* **First access**: If we access http://localhost:8080, **Nothing retrieved yet** will be shown in the last paragraph.
* **Filling the form**: If we fill the form with the text *something* and send it:
  * A 2nd request will be made (see action atribute on the form).
  * The 2nd request will ask for http://localhost:8080/index.jsp?myParameter=something.
  * This time, *something* will show in the last paragraph.
* **Manually entering the path**: If we enter the 2nd url directly or if we change the text after the '=', it will work the same way.

## Explaination

```index.jsp``` file always tries to get ```myParameter``` parameter from the url. If there is nothing, the default value will be used (**Nothing retrievet yet**).

```jsp
...
<%
    String myParameter = request.getParameter("myParameter");
        if(myParameter==null){
        myParameter = new String("Nothing retrieved yet.");
    }
%>
...
```

Then, we will print the value in the last paragraph:

```jsp
...
<p><%= myParameter %></p>
...
```

* **Remember!**, java code will be written between ```<% %>``` or ```<%= %>```. The difference is:
  * **```<% %>```**: won't print anything (as long as not specifically asked with a print or something similar).
  * **```<%= %>```**: will directly print what it is inside.

Therefore, if the request url is http://localhost:8080 (or http://localhost:8080/index.jsp), it will print **Nothing retrieved yet**.

If the request url is http://localhost:8080/index.jsp?myParameter=randomtext (or http://localhost:8080?myParameter=randomtext), it will print **randomtext**.

### The form

But why is it made automatically when we click on "Send"?

The "magic" happends with the form element:

```html
<form action="index.jsp" method="get">
        <label>Write something to send.<input type="text" name="myParameter"/></label><br/>
        <button type="submit">Send</button>
</form>
```

There are 3 important parts in here:

1. ```action="index.jsp"```: this tells the browser to send all the information of the form to ```index.jsp``` when the form is submited.
1. ```name="myParameter"```: the text that the user writes inside theat input will be sent inside a parameter named **myParameter**.
1. ```type="submit"```: this button is the one that will trigger a request to the page inside ```action``` with all the parameter inside that form.

## Exercises to test your understanding of the lecture

2 Exercises based on this example:

1. Modify the code to work the same way but changing the parameter name to *text* (it should work http://localhost:8080/index.jsp?text=randomtext and it should still work with the form).
1. Create another JSP file (e.g.: ```retrieve.jsp```). ```index.jsp``` will still contain the form, but ```retrieve.jsp``` will be the one showing the given text.

## Next and before

* Before [01-helloworld](https://gitlab.com/mgep-web-engineering-1/javaee/01-helloworld)
* Next [03-first-servlet](https://gitlab.com/mgep-web-engineering-1/javaee/03-first-servlet)